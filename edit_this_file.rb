class HelloWorld
  def initialize(name)
    @name = name.capitalize
  end

  def say_hi
    puts "Hello #{@name}!"
  end

  TODO: Remove this comment and the method below.
end
